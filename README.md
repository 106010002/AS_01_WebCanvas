# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 




## 整體
![](https://i.imgur.com/rkNUejz.png)


網頁打開後，可以看到**左邊**是各種tool，從上到下依序是
**undo｜redo brush｜eraser｜rectangle｜circle｜triangle｜text｜reset｜save image｜upload image**

而左邊則是各種個性化調整的控制區。
包含：字體、字型大小、顏色調整、粗細調整跟五個default color。


網頁一開始的會讓cursor初始成brush，顏色則是黑色。

使用者可以透過點擊左方的tool，來換cursor的icon及功能，並運用右方的各式設置來調整。

### 需要注意的調整方式

* 粗細調整是透過按+跟-來讓linewidth變粗/細。

![](https://i.imgur.com/pGCrKMw.png)

* 顏色調整的部分，再選完顏色後，需要按下旁邊的勾勾，才可以真的換顏色。（會這樣寫的原因是因為我的電腦按下color selector之後，不會有按確認的按鈕（如下圖），因此我額外做了一個按鈕來接收顏色改變的訊息。）而若是選取default的五個color，則不需要按下勾勾。

![](https://i.imgur.com/VQ8IQj8.png)




在個別功能的部分，使用方式應該都跟助教給的示範一樣。
其中，reset完後，undo最多就到reset完之後的。



### Function description

* initialize

```
<body onload = "initCanvas()">
```

上面這一行在html的code，是為了呼叫下面這個用來初始化、設定的function。
```
function initCanvas(){

    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');

    console.log("initialize...")
    console.log(Width,Height);
    canvas.width = Width;
    canvas.height = Height;

    ctx.lineWidth = line_width;
    ctx.strokeColor = StrokeColor;
    canvas.style.cursor = " url('src/cursor/brush.cur') , auto";
    CursorTool = "brush";
    
    StoreImg();

    canvas.addEventListener('mousedown', callMouseDown);
    canvas.addEventListener('mousemove', callMouseMove);
    canvas.addEventListener('mouseup', callMouseUp);
    canvas.addEventListener('click', callText);

    var imageLoader = document.getElementById('upload_btn');

    imageLoader.addEventListener('change', upload, false);
}
```

除了一些初始的設定跟element的抓取之外，最重要的是四行**addEventListener**，這次作業主要就是透過監聽event並呼叫對應function來達成這些功能。


* callMouseDown & callMouseUp & callText

```
var callMouseDown = function (e) {
    mouseDownLoc.x = e.clientX - offsetX;
    mouseDownLoc.y = e.clientY - offsetY;
    drag = true;
    SaveCurrent();
    
    callMouseMove(e);

}
```

這個function是在管滑鼠按下之後要做什麼，因為畫形狀的時候，會跟一開始的點有關係，所以每次按下去都會把那個點記起來。

不管在用brush或是畫形狀，都是在按下去到放開（mousedown->mouseup）之間，才需要連線，所以把這個期間定為drag==true，所以在按下去的時候，就需要將drag設為true。

然後因為畫形狀的時候，在還沒mouseup前，需要一直更新畫布（否則會有不想要的形狀一直疊加到畫布上），SaveCurrent就是把此刻的畫布（畫形狀前）存起來，在之後畫形狀時，只要還沒mouseup，就會一直重複畫這個狀態，也就是一直只會有一個新的形狀在畫布上。


```
var callMouseUp = function (e){
 
    loc = GetMousePos(e.clientX,e.clientY);
    drag = false;
    ctx.beginPath();
    StoreImg();
}
```
如同上述，mouseup是drag==true狀態的終點，因此這裡需要將drag設為false。
另外為了實作redo undo，因此每次mouseup時，就會將此時的canvas存起來並放進一個array裡。

```
var callText = function(e){
    if (hasInput) return;
    loc = GetMousePos(e.clientX, e.clientY)
    addText(loc);
}
```

加text用click來call


* redo & undo & StoreImg & reset

這個部分用了3個index來輔助。分別是**img_idx,img_max_idx,img_min_idx**。

```
function StoreImg(){

    let Img = ctx.getImageData(0,0,canvas.width,canvas.height);
    image_array.push(Img);

    img_idx = img_max_idx + 1;
    img_max_idx = img_idx;

    console.log(img_idx,img_max_idx);

}
```

StoreImg會將canvas的圖push進image_array，並用**img_idx**紀錄現在的canvas是哪一個，同時因為之後的redo,undo會動到**img_idx**，所以再用一個**img_max_id**來記錄array的max idx，然後因為我設定reset之後不能再回到reset之前，所以用**img_min_idx**記住能回到的最前面的idx。


```
function undo(){
    
    if(img_idx-1 >= img_min_idx){
        img_idx = img_idx - 1;

        console.log(img_idx,img_max_idx);

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();

        ctx.putImageData(image_array[img_idx],0,0);
    }


        
}
```

undo就是回到現在的index(img_idx)的前一個，會檢查是不是已經回到最前面的step了，並畫出想要的canvas。

```
function redo(){
    if(img_idx+1 <= img_max_idx){
        console.log(img_idx,img_max_idx);
        img_idx +=1;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
    
        ctx.putImageData(image_array[img_idx],0,0);

    }
        
}
```
redo則是回到現在的index(img_idx)的後一個，會檢查是不是已經回到最新的step了，並畫出想要的canvas。

```
function reset(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    StoreImg();
    img_min_idx = img_idx;
}
```

reset就是把canvas的範圍全部清空，然後因為我不希望之後可以redo到之前的canvas，所以將img_min_idx更新成reset之後的index。


* callMouseMove （包含brush/eraser/形狀）

```
    ctx.lineWidth = radius*2;
    ctx.strokeStyle = StrokeColor;
    ctx.fillStyle = fillColor;

    loc = GetMousePos(e.clientX,e.clientY);
```

一開始會更新所有用到的常數，確保之後的數值是正確的，並運用*GetMousePos* 來找修正之後的mouse座標（*GetMousePos* 就是將e.clientX/Y 利用offsetX/Y（自訂）去修正）

```
 if( loc.x >= 0 && loc.x <= canvas.width && loc.y >=0 && loc.y <= canvas.height){
        isInCanvas = true;

    }
    else{
        isInCanvas = false;
        drag = false;
        ctx.beginPath();
    }
```

因為我希望畫筆超出畫布範圍就斷線，所以會檢查他有沒有在畫布範圍內。
  
1. brush & eraser

```
if (drag && isInCanvas) {

    ctx.lineTo(loc.x, loc.y);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(loc.x,  loc.y, radius, 0, Math.PI * 2);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(loc.x,  loc.y);

    SaveCurrent();

            
}

```

因為我覺得brush跟eraser只有差在畫筆，所以寫在一起（畫筆的type的更改在其他function），都是用點跟點之間連線的方式去做。

2. shapes

```
RedoLastStep();

(loc.x > mouseDownLoc.x)? boxX = mouseDownLoc.x : boxX = loc.x;
(loc.y > mouseDownLoc.y)? boxY = mouseDownLoc.y : boxY = loc.y;

boxW = Math.abs(loc.x - mouseDownLoc.x);
boxH = Math.abs(loc.y - mouseDownLoc.y);
```

因為所有形狀的畫法都會跟mousedown的位置和現在的位置有關係，所以在shape的部分，統一更新一個box的x,y,w,h。

**Rectangle**

```
ctx.strokeRect(boxX, boxY, boxW, boxH);
```
上面更新的x,y,w,h就是畫長方形需要的x,y,w,h。

**Triangle**

```
if(loc.y > mouseDownLoc.y){
    ctx.beginPath();
    ctx.moveTo(boxX,boxY+boxH);
    ctx.lineTo(boxX+boxW/2,boxY);
    ctx.lineTo(boxX+boxW,boxY+boxH);
    ctx.closePath();
    ctx.stroke();


}
else{
    ctx.beginPath();
    ctx.moveTo(boxX,boxY);
    ctx.lineTo(boxX+boxW,boxY);
    ctx.lineTo(boxX+boxW/2,boxY+boxH);
    ctx.closePath();
    ctx.stroke();

}
```

三角形需要考慮尖端朝上還是朝下的問題，所以分兩種情形去寫。


**Circle**

```
let R = boxW;

ctx.beginPath();
ctx.arc(boxX , boxY, R, 0, Math.PI * 2);
ctx.stroke();
```
我用更新的長方形（box）的寬當成半徑，並以box的x,y為圓心。


* 改顏色

5個default color是用onclick = 來呼叫function並運用input來判斷顏色，而調色盤的顏色，則是按下勾勾之後，會用getElementByID來取value。

* 改粗細

用onclick = 來呼叫function並運用input來判斷要加粗或調細，並更改radius，在同步linewidth。

* 換tool 

用onclick = 來呼叫function並運用input來判斷要換哪種tool，cursor icon也是在這裡換的，brush跟eraser的差別也是在此處調整（"source-over" or "destination-out"）。

* upload

```
var upload = function(e){
    console.log("upload");
    var reader = new FileReader();
    reader.onload = function(e){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = e.target.result;
        console.log(e.target.result)
    }
    reader.readAsDataURL(e.target.files[0]);       
    StoreImg();

}
```
在讀完檔之後，會把畫布的長寬調整成圖片的長寬，並且因為上傳也是一個步驟，但上傳之後不會call到mouseup，所以需要額外呼叫*StoreImg*，來儲存這個步驟。


* html之工具ㄌㄧㄝ

```
<a href= "#"  type="button" class="btn_l redo" onclick="redo()"><img src="https://img.icons8.com/ios-glyphs/60/000000/redo.png"/></a>
<a href= "#"  type="button" class="btn_l undo" onclick="undo()"><img src="https://img.icons8.com/ios-glyphs/64/000000/undo.png"/></a>
<a href= "#"  type="button" class="btn_l brush" onclick="changeType('brush')"><img src="https://img.icons8.com/ios-filled/50/000000/calligraphy-brush.png"/></a>
<a href= "#"  type="button" class="btn_l eraser" onclick="changeType('eraser')"><img src="https://img.icons8.com/ios-glyphs/48/000000/eraser.png"/></a>
<a href= "#"  type="button" class="btn_l rec" onclick="changeType('rec')"><img src="https://img.icons8.com/android/48/000000/rectangle-stroked.png"/></a>
<a href= "#"  type="button" class="btn_l circle" onclick="changeType('circle')"><img src="https://img.icons8.com/material-outlined/48/000000/circled.png"/></a>

<a href= "#"  type="button" class="btn_l tri" onclick="changeType('tri')"><img src="https://img.icons8.com/android/48/000000/triangle-stroked.png"/></a>
<a href= "#"  type="butt on" class="btn_l text" onclick="changeType('text')"><img src="https://img.icons8.com/material-sharp/48/000000/text.png"/></a>
<a href= "#" type="button" class="btn_l reset" onclick="reset()"><img src="https://img.icons8.com/android/48/000000/recurring-appointment.png"/></a>

<a href= "#" class= "btn_l save" download = "download.png" id = "save_btn" type="file" onclick="save()"><img src="https://img.icons8.com/material-sharp/48/000000/save.png"/></a>
<input type = "file" class ="btn_l upload" id="upload_btn" onclick="upload"> 
```
我左邊的工具因為是使用圖片且我不希望有邊框（然後我不知道要怎麼把邊框弄掉），所以是用href，另外我想用電腦上有的圖片上傳都沒能成功顯示，所以最後是直接用link過去的方式。


### Gitlab page link

https://106010002.gitlab.io/AS_01_WebCanvas 

### Others (Optional)

    謝謝助教～辛苦了～

<style>
table th{
    width: 100%;
}
</style>
