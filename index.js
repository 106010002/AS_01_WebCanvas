let canvas;
let ctx;
let ImageData;
let drag = false;
let isInCanvas = false;
let StrokeColor = 'black';
let fillColor = 'black';

let CursorTool = 'brush';
let Width = window.innerWidth-230;
let Height = window.innerHeight;
var offsetX = 90;
var offsetY = -20;
var radius = 5;
var minR = 1;
var maxR = 50;
var startX = 0, startY=0;
var boxX=0,boxY=0,boxW=0,boxH=0;
var hasInput = true;


let line_width = radius*2;

var image_array = new Array();
var img_idx = -1;

var img_min_idx = 0;
var img_max_idx = -1;

class MouseDownCord{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

}

class Location{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

}


let mouseDownLoc = new MouseDownCord(0,0);

let loc = new Location();

function initCanvas(){

    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');

    console.log("initialize...")
    console.log(Width,Height);
    canvas.width = Width;
    canvas.height = Height;

    ctx.lineWidth = line_width;
    ctx.strokeColor = StrokeColor;
    canvas.style.cursor = " url('src/cursor/brush.cur') , auto";
    CursorTool = "brush";
    
    StoreImg();

    canvas.addEventListener('mousedown', callMouseDown);
    canvas.addEventListener('mousemove', callMouseMove);
    canvas.addEventListener('mouseup', callMouseUp);
    canvas.addEventListener('click', callText);

    var imageLoader = document.getElementById('upload_btn');

    imageLoader.addEventListener('change', upload, false);
    




}

var callMouseMove = function (e) {

    ctx.lineWidth = radius*2;
    ctx.strokeStyle = StrokeColor;
    ctx.fillStyle = fillColor;

    loc = GetMousePos(e.clientX,e.clientY);
    console.log(70+canvas.width,loc.x)
    if( loc.x >= 0 && loc.x <= canvas.width && loc.y >=0 && loc.y <= canvas.height){
        isInCanvas = true;

    }
    else{
        isInCanvas = false;
        drag = false;
        ctx.beginPath();
    }

    console.log(isInCanvas);

    if(CursorTool == "brush" ||CursorTool == "eraser" ){

        console.log(CursorTool);

        if (drag && isInCanvas) {


            
            
            console.log(loc.x,loc.y);
            

            ctx.lineTo(loc.x, loc.y);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(loc.x,  loc.y, radius, 0, Math.PI * 2);
            ctx.fill();
            ctx.beginPath();
            ctx.moveTo(loc.x,  loc.y);

            SaveCurrent();

            
        }

    }
   else{
       if(drag&&isInCanvas){
            RedoLastStep();

            (loc.x > mouseDownLoc.x)? boxX = mouseDownLoc.x : boxX = loc.x;
            (loc.y > mouseDownLoc.y)? boxY = mouseDownLoc.y : boxY = loc.y;

            boxW = Math.abs(loc.x - mouseDownLoc.x);
            boxH = Math.abs(loc.y - mouseDownLoc.y);

            if(CursorTool==="rec") {
                ctx.strokeRect(boxX, boxY, boxW, boxH);
            }

            else if(CursorTool === "tri"){
                console.log("tri");

                if(loc.y > mouseDownLoc.y){
                    ctx.beginPath();
                    ctx.moveTo(boxX,boxY+boxH);
                    ctx.lineTo(boxX+boxW/2,boxY);
                    ctx.lineTo(boxX+boxW,boxY+boxH);
                    ctx.closePath();
                    ctx.stroke();
        
        
                }
                else{
                    ctx.beginPath();
                    ctx.moveTo(boxX,boxY);
                    ctx.lineTo(boxX+boxW,boxY);
                    ctx.lineTo(boxX+boxW/2,boxY+boxH);
                    ctx.closePath();
                    ctx.stroke();
        
                }

            }
            else if(CursorTool==="circle"){

                let R = boxW;

                ctx.beginPath();
                ctx.arc(boxX , boxY, R, 0, Math.PI * 2);
                ctx.stroke();






            }

        

       }

        
        


        

   





   }

    
}



var callMouseDown = function (e) {
    mouseDownLoc.x = e.clientX - offsetX;
    mouseDownLoc.y = e.clientY - offsetY;
    drag = true;
    SaveCurrent();
    
    callMouseMove(e);

    
}

var callMouseUp = function (e){
 
    loc = GetMousePos(e.clientX,e.clientY);
    drag = false;
    ctx.beginPath();

    StoreImg();

    

}

function changeType(input){

    CursorTool = input;
    hasInput = true;

    if(CursorTool!="eraser"){
        ctx.globalCompositeOperation="source-over";
    }
    else if(CursorTool == "eraser"){
        ctx.globalCompositeOperation="destination-out";
        canvas.style.cursor = " url('src/cursor/eraser.cur') , auto";
    }

    if(CursorTool=="brush"){

        ctx.lineWidth = radius*2;
        canvas.style.cursor = " url('src/cursor/brush.cur') , auto";

    }
    else if(CursorTool == "circle")
    {

        canvas.style.cursor = " url('src/cursor/circle.cur') , auto";
    }

    else if(CursorTool == "tri")
    {

        canvas.style.cursor = " url('src/cursor/tri.cur') , auto";
    }

    else if(CursorTool == "rec")
    {

        canvas.style.cursor = " url('src/cursor/rec.cur') , auto";
    }


    else if(CursorTool == "text" ){

        hasInput = false;

        canvas.style.cursor = "text";
    }

    console.log(CursorTool);

}

function GetMousePos(x,y){

    return{ x: x - offsetX , y : y-offsetY};

}

function SaveCurrent(){

    ImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
    


}
function RedoLastStep(){


    ctx.putImageData(ImageData,0,0);

}
function save()
{
    var image = canvas.toDataURL("image/png");
  
    save_btn.href=image;
}  

function changeLineWidth(input){

    console.log("LineWidthChange", input);
    
    if(input=='b'){
        if(radius+5 < maxR){
            radius +=5;
            ctx.lineWidth = radius*2;
        }
    }
    else if(input=='t'){
        if(radius-5 > minR){
            radius -=5;
            ctx.lineWidth = radius*2;
        }
    }
    
    console.log("LineWidth", ctx.lineWidth);

}

function reset(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    StoreImg();
    img_min_idx = img_idx;
}

function save()
{
    var image = canvas.toDataURL("image/png");
  
    save_btn.href=image;
}  


function changeColor(color){

    switch(color){
        case 'k':
            ctx.strokeStyle = '#000000';
            ctx.fillStyle = '#000000';
            break;
        case 'w':
            ctx.strokeStyle = '#ffffff';
            ctx.fillStyle = '#ffffff';
            break;
        case 'r':
            ctx.strokeStyle = '#ff3c41';
            ctx.fillStyle = '#ff3c41';
            break;

        case 'b':
            ctx.strokeStyle = '#0ebeff';
            ctx.fillStyle = '#0ebeff';
            break;
        case 'g':
            ctx.strokeStyle = '#47cf73';
            ctx.fillStyle ='#47cf73';
            break;
        case 'all':
            ctx.strokeStyle = document.getElementById("colorpicker").value;
            ctx.fillStyle = document.getElementById("colorpicker").value;

        

    }

    StrokeColor = ctx.strokeStyle;
    fillColor = ctx.fillStyle;



}

function undo(){
    
    if(img_idx-1 >= img_min_idx){
        img_idx = img_idx - 1;

        console.log(img_idx,img_max_idx);

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();

        ctx.putImageData(image_array[img_idx],0,0);
    }


        
}

function redo(){
    

    if(img_idx+1 <= img_max_idx){
        console.log(img_idx,img_max_idx);
        img_idx +=1;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
    
        ctx.putImageData(image_array[img_idx],0,0);

    }
        
}

function addText(loc) {

    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (loc.x + 90) + 'px';
    input.style.top = (loc.y - 23) + 'px';

    input.onkeydown = handleEnter;

    document.body.appendChild(input);

    input.focus();

    hasInput = true;
}


function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}


function drawText(txt, x, y) {
    //ctx.textBaseline = 'top';
    //ctx.textAlign = 'left';
    var Font = document.getElementById("font_sel").value;
    var Size = document.getElementById("size_sel").value;
    console.log(Font,Size);
    ctx.font = Size +"px " + Font;
    console.log(ctx.font);
    ctx.fillText(txt, x-95, y);

    SaveCurrent();

    StoreImg();
}

function StoreImg(){

    let Img = ctx.getImageData(0,0,canvas.width,canvas.height);
    image_array.push(Img);

    img_idx = img_max_idx + 1;
    img_max_idx = img_idx;

    console.log(img_idx,img_max_idx);

}


var upload = function(e){
    console.log("upload");
    var reader = new FileReader();
    reader.onload = function(e){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = e.target.result;
        console.log(e.target.result)
    }
    reader.readAsDataURL(e.target.files[0]);    
    
    StoreImg();

    console.log(canvas.width,canvas.height);
}

var callText = function(e){
    if (hasInput) return;
    loc = GetMousePos(e.clientX, e.clientY)
    addText(loc);
}

